# coding=utf-8

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views import generic


admin.autodiscover()


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', generic.TemplateView.as_view(template_name='base.html')),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^404.html$', generic.TemplateView.as_view(template_name='404.html')),
        url(r'^500.html$', generic.TemplateView.as_view(template_name='500.html')),
    ]
    urlpatterns += staticfiles_urlpatterns()


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
